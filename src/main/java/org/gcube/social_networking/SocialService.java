package org.gcube.social_networking;

import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import org.gcube.social_networking.rest.HelloWorld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

@Path("social-service")
public class SocialService extends Application {
    private static Logger logger = LoggerFactory.getLogger(SocialService.class);

    @Override
    public Set<Class<?>> getClasses() {
        logger.info("/social-service/ here");
        final Set<Class<?>> classes = new HashSet<Class<?>>();
        // register resources and features
        classes.add(HelloWorld.class);
        return classes;
    }

}
