package org.gcube.social_networking.rest;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import org.gcube.social_networking.socialnetworking.model.shared.Comment;

import javax.ws.rs.*;
import java.util.List;

@Path("comments")
@ResourceGroup("Comments APIs")
@ResourceLabel("Comments APIs")
@RequestHeaders({
        @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class Comments {
    @POST
    @Path("/")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public void addComment(Comment comment) {

    }

    @GET
    @Path("/{id}")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public void readComment(@PathParam("id") String id) {

    }

    @PUT
    @Path("/{id}")
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public void editComment(@PathParam("id") String id, Comment comment) {

    }

    @DELETE
    @Path("/{id}")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public void deleteComment(@PathParam("id") String id) {

    }

    //the following two methods had vreid why?
    @POST
    @Path("/{id}/hashtags")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public void saveHashTagsComment(@PathParam("id") String commentId, List<String> hashtags) {

    }

    @DELETE
    @Path("/{id}/hashtags")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public void deleteHashTagsComment(@PathParam("id") String commentId, List<String> hashtags) {

    }

}
