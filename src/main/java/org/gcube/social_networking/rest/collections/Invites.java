package org.gcube.social_networking.rest.collections;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import org.gcube.social_networking.socialnetworking.model.shared.Invite;
import org.gcube.social_networking.utils.ResourceNames;
import org.gcube.social_networking.utils.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("invites")
@ResourceGroup("Invites APIs")
@ResourceLabel("Invites APIs")
@RequestHeaders({
        @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class Invites extends Collection {
    private static final Logger logger = LoggerFactory.getLogger(Invites.class);

    private Response ErrorHandler(Exception e, String action, String id){
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        logger.info("Unable to "+action+" invite {}.", id);
        logger.info(e.getMessage());
        responseBean.setMessage(e.getMessage());
        responseBean.setSuccess(false);
        status = Response.Status.INTERNAL_SERVER_ERROR;
        return Response.status(status).entity(responseBean).build();
    }


    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readInvite(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Reading invite with id {}.", id);
            return super.read(id, ResourceNames.INVITE);
        }catch(Exception e){
            return ErrorHandler(e, "fetch",id);
        }
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveInvite(@NotNull Invite invite) {
        try{
            logger.info("Saving invite with id {}.", invite.getId());
            return super.create(invite);
        }catch(Exception e){
            return ErrorHandler(e, "create",invite.getId());
        }
    }
}
