package org.gcube.social_networking.rest.collections;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import org.gcube.social_networking.socialnetworking.model.shared.*;
import org.gcube.social_networking.utils.ResourceNames;
import org.gcube.social_networking.utils.ResponseBean;
import org.gcube.social_networking.utils.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("posts")
@ResourceGroup("Posts APIs")
@ResourceLabel("Posts APIs")
@RequestHeaders({
        @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class Posts extends Collection {
    private static final Logger logger = LoggerFactory.getLogger(Posts.class);

    private Response ErrorHandler(Exception e, String action, String id){
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        logger.info("Unable to "+action+" post {}.", id);
        logger.info(e.getMessage());
        responseBean.setMessage(e.getMessage());
        responseBean.setSuccess(false);
        status = Response.Status.INTERNAL_SERVER_ERROR;
        return Response.status(status).entity(responseBean).build();
    }

    //Posts
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPost(PostWithAttachment postWithAttachment) {
        try{
            logger.info("Creating post with id {}.", postWithAttachment.getId());
            return super.create(postWithAttachment);
        }catch(Exception e){
            return ErrorHandler(e, "create",postWithAttachment.getId());
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readPost(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Reading post with id {}.", id);
            return super.read(id, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "fetch",id);
        }
    }

    @GET
    @Path("")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response readPostsWithPrivacyLevel(@QueryParam("privacy") String privacyLevel){
        try{
            if (privacyLevel == null || privacyLevel.isEmpty()){
                privacyLevel = PrivacyLevel.PORTAL.toString();
            }
            logger.info("Reading post with privacy level = " + privacyLevel );
            return super.readWithParameter(Schema.PRIVACY, privacyLevel, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "fetch","portal privacy level");
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePost(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Deleting post with id {}.", id);
            return super.delete(id,ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "delete",id);
        }
    }


    //comments
    @POST
    @Path("/{id}/comments")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createComment(@NotNull @PathParam("id") String id, @NotNull Comment comment) {
        try{
            logger.info("Creating comment with id {}.", comment.getId());
            return super.createChildOf(id, comment, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "create comment to post",id);
        }
    }

    @GET
    @Path("/{id}/comments")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getCommentsOfPost(@NotNull @PathParam("id") String id){
        try{
            logger.info("Reading comments of post with id " + id);
            return super.readChildOf(id, ResourceNames.POST, ResourceNames.COMMENT);
        }catch(Exception e){
            return ErrorHandler(e, "fetch comments of",id);
        }
    }

    @DELETE
    @Path("/{id}/comments/{commentid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCommentOfPost(@NotNull @PathParam("id") String id, @NotNull @PathParam("commentid") String commentid) {
        try{
            logger.info("Deleting comment of post with id {}.", id);
            return super.deleteChildOf(id, commentid, ResourceNames.POST, ResourceNames.COMMENT);
        }catch(Exception e){
            return ErrorHandler(e, "delete comment of",id);
        }
    }

    //Likes
    @POST
    @Path("/{id}/likes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createLike(@NotNull @PathParam("id") String id, @NotNull Like like) {
        try{
            logger.info("Creating like with id {}.", like.getId());
            return super.createChildOf(id, like, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "create like to post",id);
        }
    }


    @GET
    @Path("/{id}/likes")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getLikesOfPost(@NotNull @PathParam("id") String id){
        try{
            logger.info("Reading likes of post with id " + id);
            return super.readChildOf(id, ResourceNames.POST, ResourceNames.LIKE);
        }catch(Exception e){
            return ErrorHandler(e, "fetch likes of",id);
        }
    }




    @GET
    @Path("/{id}/attachments")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getAttachmentsOfPost(@NotNull @PathParam("id") String id){
        try{
            logger.info("Reading attachments of post with id " + id);
            return super.readChildOf(id, ResourceNames.POST, ResourceNames.Attachment);
        }catch(Exception e){
            return ErrorHandler(e, "fetch attachments of",id);
        }
    }


    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePost(@NotNull @PathParam("id") String id, @NotNull Post post) {
        try{
            logger.info("Updating post with id {}", id);
            return super.update(id, post);
        }catch(Exception e){
            return ErrorHandler(e, "update",id);
        }
    }

    @DELETE
    @Path("/{id}/likes/{likeid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response unlikePost(@NotNull @PathParam("id") String id, @NotNull @PathParam("likeid") String likeid) {
        try{
            String username = "";  //get username somehow
            logger.info("Unliking post with id {}", id);
            return super.deleteChildOfWithParameter(id, likeid, ResourceNames.USER, username, ResourceNames.POST, ResourceNames.LIKE);
        }catch(Exception e){
            return ErrorHandler(e, "unlike",id);
        }
    }
}
