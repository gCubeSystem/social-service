package org.gcube.social_networking.rest.collections;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import org.gcube.social_networking.socialnetworking.model.shared.Like;
import org.gcube.social_networking.utils.ResourceNames;
import org.gcube.social_networking.utils.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("likes")
@ResourceGroup("Likes APIs")
@ResourceLabel("Likes APIs")
@RequestHeaders({
        @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class Likes extends Collection {
    private static final Logger logger = LoggerFactory.getLogger(Likes.class);

    private Response ErrorHandler(Exception e, String action, String id){
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        logger.info("Unable to "+action+" like {}.", id);
        logger.info(e.getMessage());
        responseBean.setMessage(e.getMessage());
        responseBean.setSuccess(false);
        status = Response.Status.INTERNAL_SERVER_ERROR;
        return Response.status(status).entity(responseBean).build();
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createLike(Like like) {
        try{
            logger.info("Creating like with id {}.", like.getKey());
            return super.create(like);
        }catch(Exception e){
            return ErrorHandler(e, "create",like.getKey());
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readLike(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Reading like with id {}.", id);
            return super.read(id, ResourceNames.LIKE);
        }catch(Exception e){
            return ErrorHandler(e, "fetch",id);
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteLike(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Deleting like with id {}.", id);
            return super.delete(id,ResourceNames.LIKE);
        }catch(Exception e){
            return ErrorHandler(e, "delete",id);
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateLike(@NotNull @PathParam("id") String id, @NotNull Like like) {
        try{
            logger.info("Updating like with id {}", id);
            return super.update(id, like);
        }catch(Exception e){
            return ErrorHandler(e, "update",id);
        }
    }
}
