package org.gcube.social_networking.rest.collections;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import org.gcube.social_networking.socialnetworking.model.shared.Attachment;
import org.gcube.social_networking.utils.ResourceNames;
import org.gcube.social_networking.utils.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("attachments")
@ResourceGroup("Attachments APIs")
@ResourceLabel("Attachments APIs")
@RequestHeaders({
        @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class Attachments extends Collection {
    private static final Logger logger = LoggerFactory.getLogger(Attachments.class);

    private Response ErrorHandler(Exception e, String action, String id){
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        logger.info("Unable to "+action+" attachment {}.", id);
        logger.info(e.getMessage());
        responseBean.setMessage(e.getMessage());
        responseBean.setSuccess(false);
        status = Response.Status.INTERNAL_SERVER_ERROR;
        return Response.status(status).entity(responseBean).build();
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAttachment(Attachment attachment) {
        try{
            logger.info("Creating attachment with id {}.", attachment.getId());
            return super.create(attachment);
        }catch(Exception e){
            return ErrorHandler(e, "create",attachment.getId());
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAttachment(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Reading attachment with id {}.", id);
            return super.read(id, ResourceNames.Attachment);
        }catch(Exception e){
            return ErrorHandler(e, "fetch",id);
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAttachment(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Deleting attachment with id {}.", id);
            return super.delete(id,ResourceNames.Attachment);
        }catch(Exception e){
            return ErrorHandler(e, "delete",id);
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAttachment(@NotNull @PathParam("id") String id, @NotNull Attachment attachment) {
        try{
            logger.info("Updating attachment with id {}", id);
            return super.update(id, attachment);
        }catch(Exception e){
            return ErrorHandler(e, "update",id);
        }
    }
}
