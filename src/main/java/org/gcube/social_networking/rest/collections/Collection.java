package org.gcube.social_networking.rest.collections;

import org.gcube.social_networking.server.CassandraConnection;
import org.gcube.social_networking.socialnetworking.model.shared.Invite;
import org.gcube.social_networking.socialnetworking.model.shared.InviteOperationResult;
import org.gcube.social_networking.utils.ResourceNames;
import org.gcube.social_networking.utils.ResponseBean;
import org.gcube.social_networking.socialnetworking.model.shared.Resource;

import javax.mail.internet.AddressException;
import javax.ws.rs.core.Response;
import java.util.List;

public class Collection {

    //Create
    public Response create(Resource resource) {
        String resourceName = resource.getClass().getSimpleName();
        if (resourceName.equals(ResourceNames.INVITE)){
            ResponseBean<InviteOperationResult> responseBean = new ResponseBean<>();
            Response.Status status;
            InviteOperationResult result;
            try {
                result = CassandraConnection.getInstance().getDatabookStore().saveInvite((Invite)resource);
            } catch (AddressException e) {
                throw new RuntimeException(e);
            }
            status = (result == InviteOperationResult.SUCCESS) ? Response.Status.CREATED : (result == InviteOperationResult.ALREADY_INVITED)? Response.Status.FOUND : Response.Status.INTERNAL_SERVER_ERROR;
            responseBean.setResult(result);
            responseBean.setMessage((result == InviteOperationResult.SUCCESS) ? resourceName + " {" + resource.getId() + "} Created Successfully" : (result == InviteOperationResult.ALREADY_INVITED) ? resourceName + " {" + resource.getId() + "} already exists" : "Error");
            responseBean.setSuccess(result==InviteOperationResult.SUCCESS);
            return Response.status(status).entity(responseBean).build();
        }
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().create(resource);
        status = result ? Response.Status.CREATED : Response.Status.FOUND;
        responseBean.setResult(result);
        responseBean.setMessage(result ? resourceName + " {" + resource.getId() + "} Created Successfully" : resourceName + " {" + resource.getId() + "} already exists");
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();

    }

    public Response createChildOf(String parentid, Resource childResource, String parentResourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        String childResourceName = childResource.getClass().getSimpleName();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().createChildOf(parentid, childResource, parentResourceName);
        status = result ? Response.Status.CREATED : Response.Status.FOUND;
        responseBean.setResult(result);
        responseBean.setMessage(result ? childResourceName + " {" + childResource.getId() + "} Created Successfully for " + parentResourceName + "{" + parentid + "}."
                : childResourceName + " {" + childResource.getId() + "} already exists");
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();

    }

    public Response BatchCreateChildOfWithParameter(String parentid, String parameterName, String parameterValue, List<Resource> childResource, String parentResourceName, String childResourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().BatchCreateChildOfWithParameter(parentid, parameterName, parameterValue, childResource, parentResourceName, childResourceName);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? childResourceName + " created Successfully" : childResourceName + " cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }


    //Read
    public Response read(String id, String resourceName) {
        ResponseBean<Resource> responseBean = new ResponseBean<>();
        Response.Status status;
        Resource resource = CassandraConnection.getInstance().getDatabookStore().read(id, resourceName);
        status = resource != null ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(resource != null ? resourceName + " {" + id + "} fetched Successfully" : resourceName + " {" + id + "} cannot be found");
        responseBean.setSuccess(resource != null);
        responseBean.setResult(resource);
        return Response.status(status).entity(responseBean).build();
    }

    public Response check(String id, String parameterName, String parameterValue, String resourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().check(id, parameterName, parameterValue, resourceName);
        status = result != null ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result != null ? resourceName + " {" + id + "} checked Successfully" : resourceName + " {" + id + "} cannot be found");
        responseBean.setSuccess(result != null);
        responseBean.setResult(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response checkChildOf(String parentid, String parameterName, String parameterValue, String parentResourceName, String childResourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().checkChildOf(parentid, parameterName, parameterValue, parentResourceName, childResourceName);
        status = result != null ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result != null ? childResourceName + " {" + parentid + "} checked Successfully" : childResourceName + " {" + parentid + "} cannot be found");
        responseBean.setSuccess(result != null);
        responseBean.setResult(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response readAll(String resourceName) {
        ResponseBean<List<Resource>> responseBean = new ResponseBean<>();
        Response.Status status;
        List<Resource> resources = CassandraConnection.getInstance().getDatabookStore().readAll(resourceName);
        status = resources != null ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(resources != null ? resourceName + "s {" + "} fetched Successfully" : resourceName + "s {" + "} cannot be found");
        responseBean.setSuccess(resources != null);
        responseBean.setResult(resources);
        return Response.status(status).entity(responseBean).build();
    }

    public Response readChildOf(String parentid, String parentResourceName, String childResourceName) {
        ResponseBean<List<Resource>> responseBean = new ResponseBean<>();
        Response.Status status;
        List<Resource> resources = CassandraConnection.getInstance().getDatabookStore().readChildOf(parentid, parentResourceName, childResourceName);
        status = resources != null ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(resources != null ? childResourceName + " of " + parentResourceName + " {" + parentid + "} fetched Successfully"
                : parentResourceName + " {" + parentid + "} cannot be found");
        responseBean.setSuccess(resources != null);
        responseBean.setResult(resources);
        return Response.status(status).entity(responseBean).build();
    }

    public Response readWithParameter(String parameterName, String parameterValue, String resourceName) {
        ResponseBean<List<Resource>> responseBean = new ResponseBean<>();
        Response.Status status;
        List<Resource> resources = CassandraConnection.getInstance().getDatabookStore().readWithParameter(parameterName, parameterValue, resourceName);
        status = resources != null ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(resources != null ? resourceName + "with " + parameterName + "=" + parameterValue + " fetched Successfully"
                : resourceName + "with " + parameterName + "=" + parameterValue + " cannot be found");
        responseBean.setSuccess(resources != null);
        responseBean.setResult(resources);
        return Response.status(status).entity(responseBean).build();
    }

    public Response readChildOfWithParameter(String parentid, String parameterName, String parameterValue,
                                             String parentResourceName, String childResourceName) {
        ResponseBean<List<Resource>> responseBean = new ResponseBean<>();
        Response.Status status;
        List<Resource> resources = CassandraConnection.getInstance().getDatabookStore().readChildOfWithParameter(parentid, parameterName, parameterValue, parentResourceName, childResourceName);
        status = resources != null ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(resources != null ? childResourceName + " of " + parentResourceName + "{" + parentid + "}" + "with " + parameterName + "=" + parameterValue + " fetched Successfully"
                : childResourceName + " of " + parentResourceName + "{" + parentid + "}" + "with " + parameterName + "=" + parameterValue + " cannot be found");
        responseBean.setSuccess(resources != null);
        responseBean.setResult(resources);
        return Response.status(status).entity(responseBean).build();
    }

    //Update
    public Response update(String id, Resource resource) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        String resourceName = resource.getClass().getSimpleName();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().update(resource);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? resourceName + " {" + id + "} updated Successfully" : resourceName + " {" + id + "} cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response updateChildOf(String parentid, String childid, Resource childResource, String parentResourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        String childResourceName = childResource.getClass().getSimpleName();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().updateChildOf(parentid, childid, childResource, parentResourceName);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? childResourceName + " {" + childid + "} updated Successfully" : childResourceName + " {" + childid + "} cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response BatchUpdateChildOf(String parentid, List<Resource> childResource, String parentResourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        String childResourceName = childResource.get(0).getClass().getSimpleName();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().BatchUpdateChildOf(parentid, childResource, parentResourceName);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? childResourceName + " updated Successfully" : childResourceName + " cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response updateWithParameter(String parameterName, String parameterValue, Resource resource) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        String resourceName = resource.getClass().getSimpleName();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().updateWithParameter(parameterName, parameterValue, resource);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? resourceName + " with" + parameterName + "=" + parameterValue + " updated Successfully"
                : resourceName + " with" + parameterName + "=" + parameterValue + " cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response updateChildOfWithParameter(String parentid, String parameterName, String parameterValue,
                                               Resource childResource, String parentResourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        String childResourceName = childResource.getClass().getSimpleName();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().updateChildOfWithParameter(parentid, parameterName, parameterValue, childResource, parentResourceName);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? childResourceName + " of " + parentResourceName + "{" + parentid + "}" + "with " + parameterName + "=" + parameterValue + " updated Successfully"
                : childResourceName + " of " + parentResourceName + "{" + parentid + "}" + "with " + parameterName + "=" + parameterValue + " cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }

    //Delete
    public Response delete(String id, String resourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().delete(id, resourceName);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? resourceName + " {" + id + "} deleted Successfully" : resourceName + " {" + id + "} cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response deleteChildOf(String parentid, String childid, String parentResourceName, String childResourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().deleteChildOf(parentid, childid, childResourceName);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? childResourceName + " {" + childid + "} deleted Successfully for " + parentResourceName + "{" + parentid + "}."
                : childResourceName + " {" + childid + "} cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response deleteWithParameter(String parameterName, String parameterValue, String resourceName) {
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        Boolean result = CassandraConnection.getInstance().getDatabookStore().deleteWithParameter(parameterName, parameterValue, resourceName);
        status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
        responseBean.setMessage(result ? resourceName + " with" + parameterName + "=" + parameterValue + " deleted Successfully"
                : resourceName + " with" + parameterName + "=" + parameterValue + " cannot be found");
        responseBean.setResult(result);
        responseBean.setSuccess(result);
        return Response.status(status).entity(responseBean).build();
    }

    public Response deleteChildOfWithParameter(String parentid, String childid, String parameterName, String parameterValue, String parentResourceName, String childResourceName) {
            ResponseBean<Boolean> responseBean = new ResponseBean<>();
            Response.Status status;
            Boolean result = CassandraConnection.getInstance().getDatabookStore().deleteChildOfWithParameter(parentid, childid, parameterName, parameterValue, parentResourceName, childResourceName);
            status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
            responseBean.setMessage(result ? childResourceName + " of " + parentResourceName + "{" + parentid + "}" + "with " + parameterName + "=" + parameterValue + " deleted Successfully"
                    : childResourceName + " of " + parentResourceName + "{" + parentid + "}" + "with " + parameterName + "=" + parameterValue + " cannot be found");
            responseBean.setResult(result);
            responseBean.setSuccess(result);
            return Response.status(status).entity(responseBean).build();
    }

    public Response BatchDeleteChildOfWithParameter (String parentid, String parameterName, String
        parameterValue, List < Resource > childResource, String parentResourceName, String childResourceName){
            ResponseBean<Boolean> responseBean = new ResponseBean<>();
            Response.Status status;
            Boolean result = CassandraConnection.getInstance().getDatabookStore().BatchDeleteChildOfWithParameter(parentid, parameterName, parameterValue, childResource, parentResourceName, childResourceName);
            status = result ? Response.Status.OK : Response.Status.NOT_FOUND;
            responseBean.setMessage(result ? childResourceName + " deleted Successfully" : childResourceName + " cannot be found");
            responseBean.setResult(result);
            responseBean.setSuccess(result);
            return Response.status(status).entity(responseBean).build();
    }
}