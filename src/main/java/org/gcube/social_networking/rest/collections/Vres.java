package org.gcube.social_networking.rest.collections;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import org.gcube.social_networking.socialnetworking.model.shared.*;
import org.gcube.social_networking.utils.ParameterNames;
import org.gcube.social_networking.utils.ResourceNames;
import org.gcube.social_networking.utils.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.*;


@Path("vres")
@ResourceGroup("Vres APIs")
@ResourceLabel("Vres APIs")
@RequestHeaders({
        @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class Vres extends Collection {

    private static final Logger logger = LoggerFactory.getLogger(Vres.class);
    private Response ErrorHandler(Exception e, String action, String id){
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        logger.info("Unable to "+action+" vre {}.", id);
        logger.info(e.getMessage());
        responseBean.setMessage(e.getMessage());
        responseBean.setSuccess(false);
        status = Response.Status.INTERNAL_SERVER_ERROR;
        return Response.status(status).entity(responseBean).build();
    }

    @GET
    @Path("/{id}/posts")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getAllPostsByVre(@NotNull @PathParam("id") String id){
        try{
            logger.info("Getting all posts of VRE: " + id);
            return super.readChildOf(id, ResourceNames.VRE, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "fetch posts of",id);
        }
    }

    @GET
    @Path("/")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getAllVreIds(){
        try{
            logger.info("Getting all VRE IDs");
            List<String> ids = new ArrayList<>();
            List<Vre> vres = super.readAll(ResourceNames.VRE).readEntity(new GenericType<ResponseBean<List<Vre>>>(){}).getResult();
            for(Vre vre: vres){
                ids.add(vre.getId());
            }
            Response.Status status = Response.Status.OK;
            ResponseBean <List<String>> responseBean = new ResponseBean<>();
            responseBean.setMessage("vre ids fetched Successfully");
            responseBean.setSuccess(true);
            responseBean.setResult(ids);
            return Response.status(status).entity(responseBean).build();
        }catch(Exception e){
            return ErrorHandler(e, "fetch ids of","");
        }
    }

    @GET
    @Path("/{id}/posts")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getRecentPostsByVre(@NotNull @PathParam("id") String id, @QueryParam("limit")  @DefaultValue("10") int limit){
        try{
            logger.info("Getting most recent {} posts of VRE {} " , limit, id);
            return super.readChildOfWithParameter(id, ParameterNames.RECENT_LIMIT, String.valueOf(limit), ResourceNames.VRE, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "fetch recent posts of",id);
        }
    }

    @POST
    @Path("/{id}/posts")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response savePostToVRETimeline(@NotNull @PathParam("id") String id, @NotNull Post post){
        try{
            logger.info("saving post {} to VRE {} " , post.getKey(), id);
            return super.createChildOf(id, post, ResourceNames.VRE);
        }catch(Exception e){
            return ErrorHandler(e, "save post to",id);
        }
    }

    @GET
    @Path("/{id}/hashtags")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getVREHashtagsWithOccurrence(@NotNull @PathParam("id") String id){
        try{
            logger.info("getting hashtags of VRE {} " , id);
            return super.readChildOf(id, ResourceNames.VRE, ResourceNames.HASHTAG);
        }catch(Exception e){
            return ErrorHandler(e, "get hashtags of",id);
        }
    }


    @GET
    @Path("/{id}/hashtags")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getVREHashtagsWithOccurrenceFilteredByTime(@NotNull @PathParam("id") String id, @NotNull @QueryParam("time") long timestamp){
        try{
            logger.info("getting hashtags of VRE {} starting {} " , id, timestamp);
            return super.readChildOfWithParameter(id, ParameterNames.TIME, String.valueOf(timestamp), ResourceNames.VRE, ResourceNames.HASHTAG);
        }catch(Exception e){
            return ErrorHandler(e, "get time_filtered hashtags of",id);
        }
    }

    @GET
    @Path("/{id}/hashtags/{hashtag}/posts")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getVREPostsByHashtag(@NotNull @PathParam("id") String id, @NotNull @PathParam("hashtag") String hashtag){
        try{
            logger.info("getting posts containing hashtags in VRE {}" , id);
            return super.readChildOfWithParameter(id, ParameterNames.HASHTAG, hashtag, ResourceNames.VRE, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "get posts containing hashtag in",id);
        }
    }

    @GET
    @Path("/{id}/invites/")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getInvitedEmailsByVRE(@NotNull @PathParam("id") String id, @NotNull InviteStatus... status){
        try{
            logger.info("getting invites in VRE {}" , id);
            return super.readChildOfWithParameter(id, ParameterNames.STATUS, Arrays.toString(status), ResourceNames.VRE, ResourceNames.INVITE);
        }catch(Exception e){
            return ErrorHandler(e, "get invites in",id);
        }
    }

    @GET
    @Path("/{id}/invites/{email}")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response isExistingInvite(@NotNull @PathParam("id") String id, @NotNull @PathParam("email")String email){
        try{
            logger.info("checking if invite {} exists in VRE {}" , email, id);
            return super.readChildOfWithParameter(id, ParameterNames.EMAIL, email, ResourceNames.VRE, ResourceNames.INVITE);
        }catch(Exception e){
            return ErrorHandler(e, "check invite in",id);
        }
    }

    @PUT
    @Path("/{id}/invites/{email}")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response setInviteStatus(@NotNull @PathParam("id") String id, @NotNull @PathParam("email")String email, @NotNull InviteStatus inviteStatus){
        try{
            logger.info("setting invite {} status in VRE {}" , email, id);
            Invite invite = new Invite();
            invite.setStatus(inviteStatus);
            return super.updateChildOfWithParameter(id, ParameterNames.EMAIL, email, invite, ResourceNames.VRE);
        }catch(Exception e){
            return ErrorHandler(e, "set invite status in",id);
        }
    }


    @POST
    @Path("/{id}/posts/{postid}/hashtags")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response saveHashTags(@NotNull @PathParam("id") String id, @NotNull @PathParam("postid") String postid, @NotNull List<Hashtag>hashtags){
        try{
            logger.info("saving post hashtags to VRE {} ", id);
            return super.BatchCreateChildOfWithParameter(id, ResourceNames.POST, postid, new ArrayList<>(hashtags), ResourceNames.VRE, ResourceNames.HASHTAG);
        }catch(Exception e){
            return ErrorHandler(e, "save post hashtags to",id);
        }
    }

    @DELETE
    @Path("/{id}/posts/{postid}/hashtags")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response deleteHashTags(@NotNull @PathParam("id") String id, @NotNull @PathParam("postid") String postid, @NotNull List<Hashtag>hashtags){
        try{
            logger.info("deleting post hashtags from VRE {} " , id);
            return super.BatchDeleteChildOfWithParameter(id, ResourceNames.POST, postid, new ArrayList<>(hashtags), ResourceNames.VRE, ResourceNames.HASHTAG);
        }catch(Exception e){
            return ErrorHandler(e, "delete post hashtags from",id);
        }
    }


    @POST
    @Path("/{id}/comments/{commentid}/hashtags")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response saveHashTagsComment(@NotNull @PathParam("id") String id, @NotNull @PathParam("commentid") String commentid, @NotNull List<Hashtag>hashtags){
        try{
            logger.info("saving comment hashtags to VRE {} ", id);
            return super.BatchCreateChildOfWithParameter(id, ResourceNames.COMMENT, commentid, new ArrayList<>(hashtags), ResourceNames.VRE, ResourceNames.HASHTAG);
        }catch(Exception e){
            return ErrorHandler(e, "save comment hashtags to",id);
        }
    }

    @DELETE
    @Path("/{id}/comments/{commentid}/hashtags")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response deleteHashTagsComment(@NotNull @PathParam("id") String id, @NotNull @PathParam("commentid") String commentid, @NotNull List<Hashtag>hashtags){
        try{
            logger.info("deleting comment hashtags from VRE {} " , id);
            return super.BatchDeleteChildOfWithParameter(id, ResourceNames.COMMENT, commentid, new ArrayList<>(hashtags), ResourceNames.VRE, ResourceNames.HASHTAG);
        }catch(Exception e){
            return ErrorHandler(e, "delete comment hashtags from",id);
        }
    }
}
