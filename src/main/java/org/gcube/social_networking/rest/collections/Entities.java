package org.gcube.social_networking.rest.collections;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import org.gcube.social_networking.socialnetworking.model.shared.*;
import org.gcube.social_networking.utils.ParameterNames;
import org.gcube.social_networking.utils.ResourceNames;
import org.gcube.social_networking.utils.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("entities")
@ResourceGroup("Entities APIs")
@ResourceLabel("Entities APIs")
@RequestHeaders({
        @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class Entities extends Collection {
    private static final Logger logger = LoggerFactory.getLogger(Entities.class);
    private Response ErrorHandler(Exception e, String entity, String action, String id){
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        logger.info("Unable to "+action+ " " + entity + " {}.", id);
        logger.info(e.getMessage());
        responseBean.setMessage(e.getMessage());
        responseBean.setSuccess(false);
        status = Response.Status.INTERNAL_SERVER_ERROR;
        return Response.status(status).entity(responseBean).build();
    }

    @GET
    @Path("users/{id}/posts")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getAllPostsByUser(@NotNull @PathParam("id") String id,
                                      @QueryParam("liked") boolean liked,
                                      @QueryParam("limit") int limit){
        String action = "fetch posts of";
        try{
            if(liked){
                action = "fetch liked posts of";
                logger.info("Getting all liked posts of user: "+id);
                return super.readChildOfWithParameter(id, ParameterNames.ALL_LIKE, String.valueOf(limit),ResourceNames.USER, ResourceNames.POST);
            }
            logger.info("Getting all posts of user: " + id);
            return super.readChildOf(id, ResourceNames.USER, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "user", action,id);
        }
    }

    @GET
    @Path("apps/{id}/posts")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getAllPostsByApp(@NotNull @PathParam("id") String id){
        try{
            logger.info("Getting all posts of app: " + id);
            return super.readChildOf(id, ResourceNames.APP, ResourceNames.POST);
        }catch(Exception e){
            return ErrorHandler(e, "app", "fetch posts of",id);
        }
    }

    @GET
    @Path("users/{id}/posts/")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getRecentPosts(@NotNull @PathParam("id") String id,
                                   @NotNull @QueryParam("recent_comment") boolean recent_comment,
                                   @NotNull @QueryParam("recent_like") boolean recent_like,
                                   @NotNull @QueryParam("recent_limit") boolean recent_limit,
                                   @QueryParam("time") long time,
                                   @QueryParam("limit") int limit){
        String action = "fetch recent posts of";
        try{
            if (recent_comment){
                action = "fetch recently commented posts of";
                logger.info("Getting recently commented posts of user: " + id);
                return super.readChildOfWithParameter(id, ParameterNames.RECENT_COMMENT, String.valueOf(time), ResourceNames.USER, ResourceNames.POST);
            } else if (recent_like){
                action = "fetch recently liked posts of";
                logger.info("Getting recently liked posts of user: " + id);
                return super.readChildOfWithParameter(id, ParameterNames.RECENT_LIKE, String.valueOf(time), ResourceNames.USER, ResourceNames.POST);
            }else{
                if(recent_limit){
                    action = "fetch n recent posts of";
                    logger.info("Getting recent {} posts of user {}", limit, id);
                    return super.readChildOfWithParameter(id, ParameterNames.RECENT_LIMIT, String.valueOf(limit), ResourceNames.USER, ResourceNames.POST);
                } else{
                    action = "fetch filtered_recent posts of";
                    logger.info("Getting recent posts of user {}", id);
                    return super.readChildOfWithParameter(id, ParameterNames.TIME, String.valueOf(time), ResourceNames.USER, ResourceNames.POST);
                }
            }

        }catch(Exception e){
            return ErrorHandler(e, "user", action,id);
        }
    }

    @GET
    @Path("users/{id}/comments/")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getRecentComments(@NotNull @PathParam("id") String id,
                                      @QueryParam("time") long time){
        String action = "fetch recent comments of";
        try{
            logger.info("Getting recent comments of user {}", id);
            return super.readChildOfWithParameter(id, ParameterNames.TIME, String.valueOf(time), ResourceNames.USER, ResourceNames.COMMENT);
        }catch(Exception e){
            return ErrorHandler(e, "user", action,id);
        }
    }



    @GET
    @Path("users/{id}/notifications")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getAllNotificationByUser(@NotNull @PathParam("id") String id,
                                             @QueryParam("limit") @DefaultValue("10") int limit,
                                             @QueryParam("unread") boolean unread,
                                             @QueryParam("check") boolean check,
                                             @QueryParam("message") boolean message){
        String action = "fetch notifications of";
        try{
            if(unread){
                if(check){
                    if(message){
                        action = "check unread message notifications of";
                        logger.info("Checking unread message notifications of user: "+id);
                        return super.checkChildOf(id, ParameterNames.UNREAD_MESSAGE, String.valueOf(true), ResourceNames.USER, ResourceNames.NOTIFICATION);
                    }
                    action = "check unread notifications of";
                    logger.info("Checking unread notifications of user: " + id);
                    return super.checkChildOf(id, ParameterNames.UNREAD, String.valueOf(true),ResourceNames.USER, ResourceNames.NOTIFICATION);
                }
                action = "fetch unread notifications of";
                logger.info("Getting all unread notifications of user: " + id);
                return super.readChildOfWithParameter(id, ParameterNames.UNREAD, "", ResourceNames.USER, ResourceNames.NOTIFICATION);
            }
            logger.info("Getting all notifications of user: " + id);
            return super.readChildOfWithParameter(id, ParameterNames.RECENT_LIMIT, String.valueOf(limit), ResourceNames.USER, ResourceNames.NOTIFICATION);
        }catch(Exception e){
            return ErrorHandler(e, "user", action,id);
        }
    }

    @PUT
    @Path("users/{id}/notifications")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response setAllNotificationReadByUser(@NotNull @PathParam("id") String id){
        try{
            logger.info("Setting all notifications to read of user: " + id);
            Notification notification = new Notification();
            notification.setRead(true);
            return super.updateChildOf(id, "", notification, ResourceNames.USER);
        }catch(Exception e){
            return ErrorHandler(e, "user", "mark notifications read of",id);
        }
    }


    @GET
    @Path("users/{id}/notifications/channels")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getUserNotificationChannels(@NotNull @PathParam("id") String id,
                                                @NotNull NotificationType notificationType){
        try{
            logger.info("Getting notification channels of user {} with type {} ", id, notificationType.toString());
            return super.readChildOfWithParameter(id, ParameterNames.NOTIFICATION_TYPE, String.valueOf(notificationType), ResourceNames.USER, ResourceNames.NOTIFICATION);
        }catch(Exception e){
            return ErrorHandler(e, "user", "read notification channels of",id);
        }
    }

    @GET
    @Path("users/{id}/notifications/preferences")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response getUserNotificationPreferences(@NotNull @PathParam("id") String id){
        try{
            logger.info("Getting notification preferences of user {}", id);
            return super.readChildOf(id, ResourceNames.USER, ResourceNames.NOTIFICATION_PREFERENCES);
        }catch(Exception e){
            return ErrorHandler(e, "user", "read notification preferences of",id);
        }
    }

    @PUT
    @Path("users/{id}/notifications/preferences")
    @Produces({"application/json;charset=UTF-8", "application/vnd.api+json"})
    @Consumes({"application/json;charset=UTF-8", "application/vnd.api+json"})
    public Response setUserNotificationPreferences(@NotNull @PathParam("id") String id, @NotNull List<NotificationPreference> notificationPreferences){
        try{
            logger.info("Setting notification preferences of user {}", id);
            return super.BatchUpdateChildOf(id, new ArrayList<>(notificationPreferences), ResourceNames.USER);
        }catch(Exception e){
            return ErrorHandler(e, "user", "set notification preferences of",id);
        }
    }


}
