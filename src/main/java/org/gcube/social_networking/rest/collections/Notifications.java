package org.gcube.social_networking.rest.collections;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import org.gcube.social_networking.socialnetworking.model.shared.Notification;
import org.gcube.social_networking.utils.ResourceNames;
import org.gcube.social_networking.utils.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("notifications")
@ResourceGroup("Notifications APIs")
@ResourceLabel("Notifications APIs")
@RequestHeaders({
        @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class Notifications extends Collection {
    private static final Logger logger = LoggerFactory.getLogger(Notifications.class);

    private Response ErrorHandler(Exception e, String action, String id){
        ResponseBean<Boolean> responseBean = new ResponseBean<>();
        Response.Status status;
        logger.info("Unable to "+action+" notification {}.", id);
        logger.info(e.getMessage());
        responseBean.setMessage(e.getMessage());
        responseBean.setSuccess(false);
        status = Response.Status.INTERNAL_SERVER_ERROR;
        return Response.status(status).entity(responseBean).build();
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createNotification(Notification notification) {
        try{
            logger.info("Creating notification with id {}.", notification.getId());
            return super.create(notification);
        }catch(Exception e){
            return ErrorHandler(e, "create",notification.getId());
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setNotificationToRead(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Setting notification with id {} to read.", id);
            Notification notification = new Notification();
            notification.setKey(id);
            return super.update(id, notification);
        }catch(Exception e){
            return ErrorHandler(e, "update",id);
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readNotification(@NotNull @PathParam("id") String id) {
        try{
            logger.info("Reading notification with id {}.", id);
            return super.read(id, ResourceNames.NOTIFICATION);
        }catch(Exception e){
            return ErrorHandler(e, "fetch",id);
        }
    }
}
