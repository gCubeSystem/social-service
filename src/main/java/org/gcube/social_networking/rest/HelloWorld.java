package org.gcube.social_networking.rest;

import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.social_networking.SocialServiceApplicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("test")
@ManagedBy(SocialServiceApplicationManager.class)
public class HelloWorld {
	private static Logger logger = LoggerFactory.getLogger(HelloWorld.class);

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public String test(){
		logger.info("/social-service/test/ here");
		return "{\"result\":\"funziona!!!\"}";
	}

}
