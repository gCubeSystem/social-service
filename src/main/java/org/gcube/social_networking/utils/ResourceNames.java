package org.gcube.social_networking.utils;

import org.gcube.social_networking.socialnetworking.model.shared.*;

public class ResourceNames {
    public static final String NOTIFICATION = Notification.class.getSimpleName();

    public static final String POST = Post.class.getSimpleName();
    public static final String COMMENT = Comment.class.getSimpleName();
    public static final String LIKE = Like.class.getSimpleName();
    public static final String INVITE = Invite.class.getSimpleName();
    public static final String HASHTAG = Hashtag.class.getSimpleName();
    public static final String Attachment = Attachment.class.getSimpleName();
    public static final String VRE = Vre.class.getSimpleName();
    public static final String USER = "User";
    public static final String APP = "App";
    public static final String NOTIFICATION_PREFERENCES = NotificationPreference.class.getSimpleName();

}
