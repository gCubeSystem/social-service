package org.gcube.social_networking.utils;

public class ParameterNames {
    public static final String RECENT_LIMIT = "recent_limit";
    public static final String RECENT_COMMENT = "recent_comment";
    public static final String UNREAD = "unread";
    public static final String UNREAD_MESSAGE = "unread_message";
    public static final String RECENT_LIKE = "recent_like";
    public static final String ALL_LIKE = "all_like";
    public static final String TIME = "time";
    public static final String HASHTAG = "hashtag";
    public static final String STATUS = "status";
    public static final String EMAIL = "email";
    public static final String NOTIFICATION_TYPE = "notification_type";
}
