package org.gcube.social_networking.server;

import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;

import org.gcube.social_networking.socialnetworking.model.shared.Resource;
import org.gcube.social_networking.socialnetworking.model.shared.Invite;
import org.gcube.social_networking.socialnetworking.model.shared.InviteOperationResult;


/**
 * @author Massimiliano Assante ISTI-CNR
 * @author Costantino Perciante ISTI-CNR
 * <class>DatabookStore</class> is the high level interface for querying and adding data to DatabookStore
 */
public interface SocialDBDriver {
    /**
     * create a resource in the DB
     */
    Boolean create(Resource resource);
    /**
     * create a child resource given its parent id in the DB
     */
    Boolean createChildOf(String parentid, Resource childResource, String parentResourceName);
    /**
     * batch create a child resource given its parent id in the DB
     */
    Boolean BatchCreateChildOfWithParameter(String parentid, String parameterName, String parameterValue, List<Resource> childResource, String parentResourceName, String childResourceName);

    /**
     * read a resource given its id from the DB
     */
    Resource read(String id, String resourceName);
    /**
     * check attribute value of a resource given its id from the DB
     */
    Boolean check(String id, String parameterName, String parameterValue, String resourceName);
    /**
     * check attribute value of a child resource given its parent id from the DB
     */
    Boolean checkChildOf(String parentid, String parameterName, String parameterValue, String parentResourceName, String childResourceName);
    /**
     * list all instances of a resource in the DB
     */
    List<Resource> readAll(String resourceName);
    /**
     * read a list of the children of a resource given its id from the DB
     */
    List<Resource>readChildOf(String parentid, String parentResourceName, String childResourceName);
    /**
     * read a list of resources with a specific attribute value
     */
    List<Resource> readWithParameter(String parameterName, String parameterValue, String resourceName);
    /**
     * read a list of the children of a resource with a specific attribute value
     */
    List<Resource> readChildOfWithParameter(String parentid, String parameterName, String parameterValue, String parentResourceName, String childResourceName);
    /**
     * update a resource given its id in the DB
     */
    Boolean update(Resource resource);
    /**
     * update a child resource given its id and its parent id in the DB
     */
    Boolean updateChildOf(String parentid, String childid, Resource childResource, String parentResourceName);
    /**
     * Batch update a child resource given its id and its parent id in the DB
     */
    Boolean BatchUpdateChildOf(String parentid, List<Resource> childResource, String parentResourceName);
    /**
     * update a resource with a certain attribute value
     */
    Boolean updateWithParameter(String parameterName, String parameterValue, Resource resource);
    /**
     * update a child resource with a certain attribute value given its parent id
     */
    Boolean updateChildOfWithParameter(String parentid, String parameterName, String parameterValue, Resource childResource,String parentResourceName);

    /**
     * delete a resource given its id in the DB
     */
    Boolean delete(String id, String resourceName);
    /**
     * delete a child resource given its id and its parent id in the DB
     */
    Boolean deleteChildOf(String parentid, String childid, String childResourceName);
    /**
     * delete resources with a certain attribute value
     */
    Boolean deleteWithParameter(String parameterName, String parameterValue, String resourceName);
    /**
     * delete a child resource with a certain attribute value given its parent id
     */
    Boolean deleteChildOfWithParameter(String parentid, String childid, String parameterName, String parameterValue, String parentResourceName, String childResourceName);
    /**
     * batch delete a child resource with a certain attribute value given its parent id
     */
    Boolean BatchDeleteChildOfWithParameter(String parentid, String parameterName, String parameterValue, List<Resource> childResource, String parentResourceName, String childResourceName);






    //RangePosts getRecentPostsByVREAndRange(String vreid, int from, int quantity) throws PrivacyLevelTypeNotFoundException, PostTypeNotFoundException, ColumnNameNotFoundException, PostIDNotFoundException;
    //List<Notification> getRangeNotificationsByUser(String userid, int from, int quantity) throws NotificationTypeNotFoundException,	ColumnNameNotFoundException, NotificationIDNotFoundException;


    InviteOperationResult saveInvite(Invite invite) throws AddressException;

    /**
     * close the connection to the underlying database
     */
    void closeConnection();


}

