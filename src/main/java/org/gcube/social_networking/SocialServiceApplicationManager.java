package org.gcube.social_networking;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.smartgears.ApplicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SocialServiceApplicationManager implements ApplicationManager {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(SocialServiceApplicationManager.class);
	
	public static boolean initialised;
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void onInit() {

		logger.info("Starting social service");
		try{
			String context = SecretManagerProvider.instance.get().getContext();
			logger.info(
					"\n-------------------------------------------------------\n"
							+ "Social Service is Starting on context {}\n"
							+ "-------------------------------------------------------",
					context);
		}catch (Throwable e) {
			logger.error("unexpected error initiliazing storagehub",e);
		}

		

		
//		ApplicationContext applicationContext = ContextProvider.get();
//		String helloWorldEServiceID  = applicationContext.id();

	}
	
	/** 
	 * {@inheritDoc} 
	 */
	@Override
	public synchronized void onShutdown(){
		
		String context = SecretManagerProvider.instance.get().getContext();
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "Hello World Service is Stopping on context {}\n"
				+ "-------------------------------------------------------", 
				context);
		
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "Hello World Service Stopped Successfully on context {}\n"
				+ "-------------------------------------------------------", 
				context);
	}
}
