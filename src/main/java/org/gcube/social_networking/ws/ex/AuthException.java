package org.gcube.social_networking.ws.ex;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

public class AuthException extends WebApplicationException  {
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthException(Throwable cause) {
		super(cause, Status.FORBIDDEN);
	}

}