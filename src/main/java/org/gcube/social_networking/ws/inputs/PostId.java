package org.gcube.social_networking.ws.inputs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Application id object

 */
public class PostId {

	@JsonProperty("postid")
	@NotNull(message="postid cannot be null")
	@Size(message="postid cannot be empty", min=1)
	private String postId;

	public PostId() {
		super();
	}

	public PostId(String postId) {
		super();
		this.postId = postId;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	@Override
	public String toString() {
		return "PostId [postid=" + postId + "]";
	}

	
}
