package org.gcube.social_networking.ws.methods.v2;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portal.databook.shared.ApplicationProfile;
import org.gcube.social_networking.ws.inputs.ApplicationId;
import org.gcube.social_networking.ws.outputs.ResponseBean;
import org.gcube.social_networking.ws.utils.ErrorMessages;
import org.gcube.social_networking.ws.utils.SocialUtils;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;


/**
 * REST interface for the social networking library (tokens).
 */
@Path("2/tokens")
@Deprecated
public class Tokens {

	// Logger
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Tokens.class);

	@POST
	@Path("generate-application-token/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@StatusCodes ({
		@ResponseCode ( code = 201, condition = "Successful creation of the token, reported in the 'result' field of the returned object"),
		@ResponseCode ( code = 403, condition = "There is no application profile with such id"),
		@ResponseCode ( code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	/**
	 * Generate a legacy application auth token for the application with id app_id
	 * @param input The bean containing the app_id field
	 * @return the legacy application token
	 * @throws ValidationException
	 */
	public Response generateApplicationToken(
			@NotNull(message="Missing input parameter") 
			@Valid
			ApplicationId input) throws ValidationException{

		logger.debug("Incoming request for app token generation.");
		String context = ScopeProvider.instance.get();
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.CREATED;
		String appId = input.getAppId();
		try {
			// check if the token actually matches an application profile
			ApplicationProfile appProfile = SocialUtils.getProfileFromInfrastrucure(appId, context);
			if(appProfile == null){
				logger.error("The given id doesn't belong to an application!!!");
				responseBean.setSuccess(false);
				responseBean.setMessage(ErrorMessages.NOT_APP_ID);
				status = Status.FORBIDDEN;
				return Response.status(status).entity(responseBean).build();
			}
			logger.debug("Generating token for the application with id " + appId);
			// each token is related to an identifier and the context
			String appToken = authorizationService().generateExternalServiceToken(appId);
			responseBean.setSuccess(true);
			responseBean.setResult(appToken);
		} catch (Exception e) {
			logger.error("Unable to generate token for app " + appId + " and scope " + context);
			status = Status.INTERNAL_SERVER_ERROR;
			responseBean.setSuccess(false);
			responseBean.setMessage(ErrorMessages.TOKEN_GENERATION_APP_FAILED);
			return Response.status(status).entity(responseBean).build();
		}

		return Response.status(status).entity(responseBean).build();
	}

}
