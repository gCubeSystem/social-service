package org.gcube.social_networking.ws.methods.v2;


import java.util.List;

import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.utils.Caller;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portal.databook.shared.Like;
import org.gcube.social_networking.ws.inputs.LikeInputBean;
import org.gcube.social_networking.ws.inputs.PostId;
import org.gcube.social_networking.ws.outputs.ResponseBean;
import org.gcube.social_networking.ws.utils.CassandraConnection;
import org.gcube.social_networking.ws.utils.ErrorMessages;
import org.gcube.social_networking.ws.utils.SocialUtils;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * REST interface for the social networking library (likes).
 * @author Ahmed Ibrahim ISTI-CNR
 */
@Path("2/likes")
@RequestHeaders ({
		@RequestHeader( name = "Authorization", description = "Bearer token, see https://dev.d4science.org/how-to-access-resources"),
		@RequestHeader( name = "Content-Type", description = "application/json")
})
public class Likes {

	// Logger
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Likes.class);

	/*
	 * Retrieve the list of likes belonging to the post id (key) of the token in the related context
	 * @param key the key as in the POST JSON representation
	 * @pathExample /get-likes-by-post-id?key=9ea137e9-6606-45ff-a1a2-94d4e8760583
	 * @return the list of likes belonging to the post identified by the key in the context identified by the token
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get-likes-by-post-id")
	@StatusCodes ({
			@ResponseCode ( code = 200, condition = "The list of likes is put into the 'result' field"),
			@ResponseCode ( code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	public Response getAllLikesByPostId(
			@NotNull
			@QueryParam("key")
			String key) {

		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		Caller caller = AuthorizationProvider.instance.get();
		String context = ScopeProvider.instance.get();
		String username = caller.getClient().getId();
		List<Like> likes = null;

		try{
			logger.debug("Retrieving likes for user id " + username);
			likes = CassandraConnection.getInstance().getDatabookStore().getAllLikesByPost(key);
			responseBean.setResult(likes);
			responseBean.setSuccess(true);
		}catch(Exception e){
			logger.error("Unable to retrieve such likes.", e);
			responseBean.setMessage(e.getMessage());
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
		}

		return Response.status(status).entity(responseBean).build();
	}

	/**
	 * Create a new like to a post in the context of the token
	 * @param postid The post id to be liked
	 * @return true if everything is OK
	 * @throws ValidationException
	 */
	@POST
	@Path("like-post")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@StatusCodes ({
			@ResponseCode ( code = 201, condition = "Successful created, the like operation result is reported in the 'result' field of the returned object"),
			@ResponseCode ( code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	public Response like(
			@NotNull(message="Post to like is missing")
			@Valid
			PostId post) throws ValidationException {
		Caller caller = AuthorizationProvider.instance.get();
		String username = caller.getClient().getId();
		logger.debug("Request of like coming from user " + username);
		String context = ScopeProvider.instance.get();
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		boolean likeResultOperation = SocialUtils.like(username, post.getPostId(), context);
		if (likeResultOperation)
			logger.debug("Added like OK to postId " + post.getPostId());
		else {
			logger.error("Unable to like this post"+ post.getPostId());
			responseBean.setMessage("Unable to like, something went wrong please see server log");
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
			return Response.status(status).entity(responseBean).build();
		}

		responseBean.setResult(true);
		responseBean.setSuccess(true);
		return Response.status(status).entity(responseBean).build();
	}

	/**
	 * Unlike to a post in the context of the token
	 * @param postid The post id to be liked
	 * @return true if everything is OK
	 * @throws ValidationException
	 */
	@POST
	@Path("unlike-post")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@StatusCodes ({
			@ResponseCode ( code = 201, condition = "The unlike operation result is reported in the 'result' field of the returned object"),
			@ResponseCode ( code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
	})
	public Response unlike(
			@NotNull(message="Post to unlike is missing")
			@Valid
			LikeInputBean likeInputBean) throws ValidationException {
		Caller caller = AuthorizationProvider.instance.get();
		String username = caller.getClient().getId();
		logger.debug("Request of unlike coming from user " + username);
		ResponseBean responseBean = new ResponseBean();
		Status status = Status.OK;
		boolean likeResultOperation = SocialUtils.unlike(username, likeInputBean.getLikeid(), likeInputBean.getPostid());
		if (likeResultOperation)
			logger.debug("Unlike OK to postId " + likeInputBean.getPostid());
		else {
			logger.error("Unable to unlike this post"+ likeInputBean.getPostid());
			responseBean.setMessage("Unable to unlike, something went wrong please see server log");
			responseBean.setSuccess(false);
			status = Status.INTERNAL_SERVER_ERROR;
			return Response.status(status).entity(responseBean).build();
		}

		responseBean.setResult(true);
		responseBean.setSuccess(true);
		return Response.status(status).entity(responseBean).build();
	}
}
