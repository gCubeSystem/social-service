#FROM d4science/smartgears-distribution:4.0.0-SNAPSHOT-java11-tomcat9
FROM smartgears-distribution:4.0.0-java11-tomcat9

ARG REPOUSER=admin
ARG REPOPWD=admin

COPY ./target/social-service.war /usr/local/tomcat/webapps/

COPY ./docker/logback.xml /etc/
COPY ./docker/container.ini /etc/
RUN unzip /usr/local/tomcat/webapps/social-service.war -d /usr/local/tomcat/webapps/social-service
RUN rm /usr/local/tomcat/webapps/social-service.war
EXPOSE 8080
